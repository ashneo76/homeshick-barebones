# Configure pinentry to use the correct TTY
GPG_TTY=$(tty)
export GPG_TTY
timeout --kill-after 5 -s TERM 5 gpg-connect-agent updatestartuptty /bye >/dev/null

if [[ $(uname -s) == Darwin ]]; then
    source ~/imports/work.sh
    source ~/.activecampaign/ac-platform/shell-init.sh
    dv() {
        pushd ~/devenv; ~/devenv/run.sh; popd
    }

    dve() {
        pushd ~/devenv; vim Dockerfile; ~/devenv/run.sh; popd
    }
fi
[[ -f ~/.bash_local ]] && source ~/.bash_local
