export SHELL_TYPE=zsh
# exit fast when in tramp or dumb terminal
[[ $TERM == "tramp" || $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && exit

# load profiling
# export PROFILE_ZSH=1
if [[ -n ${PROFILE_ZSH:-} ]]; then
    zmodload zsh/zprof
fi

export ZSH=$HOME/.zsh
export ZPLUG_HOME=$HOME/.zplug
export ZINIT_HOME=$HOME/.zinit

# {{{ standard zsh configuration

# Initialize completion cache
zstyle ':completion::complete:*' use-cache 1

# History settings
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS
setopt HIST_IGNORE_SPACE
setopt SHARE_HISTORY
setopt INC_APPEND_HISTORY
# unsetopt SHARE_HISTORY

# compinit
autoload -Uz compinit
for dump in ~/.zcompdump(N.mh+24); do
  compinit
done
compinit -C

# Initialize bash completions
autoload -U +X bashcompinit; bashcompinit

# Initialize editor
# Allows editing the current command line in an editor
# Uses the default binding: ^X^E
autoload edit-command-line; zle -N edit-command-line

autoload -Uz promptinit
promptinit
prompt redhat

fpath=(${ZSH}/completions ${fpath})

# emacs bindings
bindkey -e

# Case insensitive match
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*:*:*:*:*' menu select

# zstyle ':completion:*' completer _expand _complete _ignored _approximate # NOTE: Broken
# zstyle ':completion:*' format ' %F{yellow}-- %d --%f'
# zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# zstyle ':completion:*' menu select=2
# zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
# zstyle ':completion:*' verbose yes
#
# zstyle ':completion:*:corrections' format ' %F{green}-- %d (errors: %e) --%f'
#
# zstyle ':completion:*:default' list-prompt '%S%M matches%s'
#
# zstyle ':completion:*:descriptions' format ' %F{yellow}-- %d --%f'
# zstyle ':completion:*:descriptions' format '-- %d --'
zstyle ':completion:*:descriptions' format '[%d]'

zstyle ':completion:*:git-checkout:*' sort false

# zstyle ':completion:*:matches' group 'yes'
# zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
# zstyle ':completion:*:options' auto-description '%d'
# zstyle ':completion:*:options' description 'yes'
#
# zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm,cmd -w -w"
# zstyle ':completion:*:processes' command 'ps -au$USER'
#
# zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
# zstyle ':completion:complete:*:options' sort false

# # fzf-tab settings
zstyle ':fzf-tab:*' switch-group ',' '.'
zstyle ':fzf-tab:complete:_zlua:*' query-string input
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls -1 $realpath'
zstyle ':fzf-tab:complete:mv:*' fzf-preview 'fzf-preview $realpath'
zstyle ':fzf-tab:complete:rm:*' fzf-preview 'fzf-preview $realpath'
zstyle ':fzf-tab:complete:cp:*' fzf-preview 'fzf-preview $realpath'
zstyle ':fzf-tab:complete:vim:*' fzf-preview '[[ -f $realpath ]] && ! is-binary-file $realpath && { bat $realpath || cat $realpath }'
zstyle ':fzf-tab:complete:mplayer:*' fzf-preview '[[ -f $realpath ]] && is-binary-file $realpath && ffprobe -hide_banner $realpath'
zstyle ':fzf-tab:complete:mpv:*' fzf-preview '[[ -f $realpath ]] && is-binary-file $realpath && ffprobe -hide_banner $realpath'
zstyle ':fzf-tab:complete:ffprobe:*' fzf-preview '[[ -f $realpath ]] && is-binary-file $realpath && ffprobe -hide_banner $realpath'
zstyle ':fzf-tab:complete:evince:*' fzf-preview '[[ $realpath =~ ^.*pdf$ ]]'
# zstyle ':fzf-tab:complete:systemctl:*' fzf-preview 'systemctl cat $realpath'
# zstyle ':fzf-tab:complete:kill:argument-rest' extra-opts --preview=$extract'ps --pid=$in[(w)1] -o cmd --no-headers -w -w' --preview-window=down:3:wrap

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

# # print directory listing on empty lines
# accept-line() {: "${BUFFER:="ls -lah"}"; zle ".$WIDGET"}
# zle -N accept-line

my_precmd_fn() {
  export LAST_EXIT_CODE=$?
  # fc -Dl -1
  # [ -n ${TRACE:-} ] && echo "--------- $(date) ---------\n"
  # git status >${TMPDIR}/git-status-$(readlink -f $PWD | sed -E "s/\//-/g") 2>/dev/null || :
  if command -v guile >/dev/null 2>&1 && [ -f ${HOME}/bin/prompt.scm ]; then
      export PROMPT=$(guile ${HOME}/bin/prompt.scm zsh $LAST_EXIT_CODE left $COLUMNS)
      export RPROMPT=$(guile ${HOME}/bin/prompt.scm zsh $LAST_EXIT_CODE right $COLUMNS)
      export ZSH_PROMPT_PARSER=guile
  elif command -v bb >/dev/null 2>&1 && [ -f ${HOME}/bin/prompt.clj ]; then
      export PROMPT=$(bb ${HOME}/bin/prompt.clj zsh $LAST_EXIT_CODE left $COLUMNS)
      export RPROMPT=$(bb ${HOME}/bin/prompt.clj zsh $LAST_EXIT_CODE right $COLUMNS)
      export ZSH_PROMPT_PARSER=bb
  else
    export PROMPT="$ "
    export RPROMPT=$LAST_EXIT_CODE
  fi
}

my_preexec_fn() {
  # [ -n ${TRACE:-} ] && echo "--------- $(date) ---------"
  if [[ $ZSH_PROMPT_PARSER == guile ]]; then
    envrc_dir=$(find-envrc-dir)
  else
    envrc_dir=.
  fi
  if [[ "${1}" =~ ^cd.* && -f ${TMPDIR}/envrc-${PWD//\//-} ]]; then
    source ${TMPDIR}/envrc-${PWD//\//-}
    sed 's| | -|g;s| -$||' ${TMPDIR}/envrc-${PWD//\//-}
    rm ${TMPDIR}/envrc-${PWD//\//-}
  fi
}

my_chpwd_fn() {
  if [[ $ZSH_PROMPT_PARSER == guile ]]; then
    envrc_dir=$(find-envrc-dir)
  else
    envrc_dir=.
  fi
  if [[ -n ${envrc_dir}/.envrc && -f ${TMPDIR}/envrc-${envrc_dir//\//-} ]]; then
    source ${envrc_dir}/.envrc
    echo "unset $(grep export ${envrc_dir}/.envrc | sed -E "s|export (.*)=.*|\1|" | tr '\n' ' ')" > ${TMPDIR}/envrc-${envrc_dir//\//-}
    sed 's|unset|export:|;s| | +|g;s| +$||' ${TMPDIR}/envrc-${envrc_dir//\//-}
  fi
}

function zshaddhistory() {
  if [[ $1 =~ "^ " ]]; then
    return 0
  # elif [[ $1 =~ "cp\ *|mv\ *|rm\ *|cat\ *\>|pv\ *|dd\ *" ]]; then
  #   1="# $1"
  fi
  # write to usual history location
  print -sr -- ${1%%$'\n'}
  # do not save the history line. if you have a chain of zshaddhistory
  # hook functions, this may be more complicated to manage, depending
  # on what those other hooks do (man zshall | less -p zshaddhistory)
  return 1
}

# set RPROMT directly. Will override other functions where it gets set
setopt PROMPT_SUBST
autoload -U add-zsh-hook

# }}}

# {{{ post init method

post_init() {

# {{{ tmux start

# # Perform tmux check
# if [[ -f ${HOME}/.tmux-path ]]; then
#     TMUX_BIN=$(cat ${HOME}/.tmux-path)
# else
#     TMUX_BIN=tmux
# fi
#
# if [[ -z "${TMUX+x}" ]]; then
#     N_SESS=$(${TMUX_BIN} ls 2>&1 | grep -cvE "no server|error connecting")
#     if (( N_SESS == 0 )); then
#         DISPLAY=$DISPLAY ${TMUX_BIN} -u new-session
#     else
#         # get the first detached session
#         DET_SESSION=$(${TMUX_BIN} ls 2>&1 | grep -vE 'no server|error connecting|attached' | awk -F':' '{print $1}' | head -1)
#         if [[ -n ${DET_SESSION} ]]; then
#             DISPLAY=$DISPLAY ${TMUX_BIN} -u attach -t ${DET_SESSION}
#         fi
#     fi
# fi

# }}}

# {{{ common functions

# install-antigen
install_antigen() {
    if [[ ! -f $ZSH/antigen.zsh ]]; then
        antigen_update
    fi
}

install_zplug() {
    if [[ ! -d $ZPLUG_HOME ]]; then
        git clone https://github.com/zplug/zplug $ZPLUG_HOME
    fi
}

install_zinit() {
    mkdir -p ${ZINIT_HOME}
    if [[ ! -d ${ZINIT_HOME}/bin ]]; then
        git clone https://github.com/zdharma-continuum/zinit.git ${ZINIT_HOME}/bin
    fi
}

antigen_update() {
    curl -L git.io/antigen > $ZSH/antigen.zsh
}

# hs-clone
hs-clone() {
    repo=${1}
    if [[ ! -z ${repo} ]]; then
        homeshick clone git@gitlab.com:ashiiish/homeshick-${repo}.git
    fi
}

# }}}

# {{{ omz settings

# ZSH_THEME="agnoster"
# ZSH_THEME="geoffgarside"
# ZSH_THEME="nanotech"
ZSH_THEME="af-magic"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# }}}

# {{{ antigen setup

antigen_setup() {
    install_antigen
    source $ZSH/antigen.zsh

    # enable omz
    antigen use oh-my-zsh

    # Bundles from the default repo (robbyrussell's oh-my-zsh).
    plugins=(
    git python node vagrant docker rsync lein pip systemd
    rust gem cargo postgres yarn cabal adb golang
    pyenv command-not-found zsh-hooks/zsh-hooks)
    for plugin in "${plugins[@]}"; do
        antigen bundle ${plugin}
    done
    antigen bundle hlissner/zsh-autopair

    # Syntax highlighting bundle.
    antigen bundle zsh-users/zsh-syntax-highlighting

    # Load the theme.
    antigen theme $ZSH_THEME

    # Tell Antigen that you're done.
    antigen apply
}

# antigen_setup

# }}}

# {{{ zplug setup

zplug_setup() {
    install_zplug
    source $ZPLUG_HOME/init.zsh

    zplug "mafredri/zsh-async", from:github, defer:0
    zplug "lib/completion", from:oh-my-zsh, defer:0
    zplug "romkatv/zsh-defer", from:github, defer:0
    zplug "hlissner/zsh-autopair", defer:3
    zplug "zsh-users/zsh-syntax-highlighting", defer:3
    zplug "zsh-users/zsh-completions", defer:3
    zplug "zsh-users/zsh-autosuggestions", defer:3
    zplug "zsh-hooks/zsh-hooks"
    # zplug "sindresorhus/pure", use:pure.zsh, from:github, as:theme, defer:0

    export EMOJI_CLI_KEYBIND="^n"
    zplug "b4b4r07/emoji-cli", defer:3

    # Install plugins if there are plugins that have not been installed
    if ! zplug check --verbose; then
        printf "Install? [y/N]: "
        if read -q; then
            echo; zplug install
        fi
    fi

    # Then, source plugins and add commands to $PATH
    zplug load

    zsh-defer cfg_custom_hook
}

# }}}

# {{{ zinit setup

zinit_setup() {
    install_zinit

    source ${ZINIT_HOME}/bin/zinit.zsh
    autoload -Uz _zinit
    (( ${+_comps} )) && _comps[zinit]=_zinit

    zinit light romkatv/zsh-defer

    zsh-defer zinit_configure

    # zinit light sindresorhus/pure
    zinit light zsh-hooks/zsh-hooks
}

zinit_configure() {
  zinit light mafredri/zsh-async
  zinit light hlissner/zsh-autopair
  zinit light zdharma-continuum/fast-syntax-highlighting
  zinit light zdharma-continuum/history-search-multi-word
  zinit light zsh-users/zsh-autosuggestions
  zinit light zsh-users/zsh-completions
  zinit light zsh-users/zsh-history-substring-search
  zinit light zsh-users/zsh-syntax-highlighting
  zinit light Aloxaf/fzf-tab

  zinit ice compile'(pure|async).zsh' pick'async.zsh' src'pure.zsh'

  # OMZ plugins
  declare -a omz_plugins=(
    git kubectl
  )
  for p in ${omz_plugins[@]}; do
    zinit snippet OMZP::${p}
  done

  declare -a omz_plugins_2=(lein docker)
  for p in ${omz_plugins_2[@]}; do
    zinit snippet OMZ::plugins/${p}/_${p}
  done

  declare -a omz_library=(clipboard directories grep history key-bindings spectrum)
  for l in ${omz_library[@]}; do
    zinit snippet OMZL::${l}.zsh
  done

  # async framework setup
  ZSH_ASYNC_COMPLETED_JOBS=0
  async_completed_callback() {
    ZSH_ASYNC_COMPLETED_JOBS=$(( ZSH_ASYNC_COMPLETED_JOBS + 1 ))
    print $@
  }
  async_start_worker zsh_worker -n
  async_register_callback zsh_worker async_completed_callback

  zsh-defer cfg_custom_hook

  # zinit snippet OMZP::${ZSH_THEME}

  zinit_init
}

zinit_init() {
  if [[ -L ${ZINIT_HOME}/completions/_homeshick ]]; then
    zinit creinstall ${ZSH}/completions
  fi
}

# }}}

# {{{ bracket highlighting

zmodload zsh/zselect
SHOWMATCH_GR=fg=black,bg=yellow,bold

showmatch() {
  emulate -L zsh
  set -o rematchpcre
  local -A pair=(
    ']' '['
    ')' '('
    '}' '{'
  )
  local MBEGIN

  zle .self-insert
  if [[ $LBUFFER =~ "(\\$pair[$KEYS](?:[^$KEYS$pair[$KEYS]]++|(?1))*\\$KEYS)\$" ]]; then
    local oldCURSOR=$CURSOR
    local -a old_region_highlight=($region_highlight)
    region_highlight+=("$((MBEGIN-1)) $CURSOR $SHOWMATCH_GR")
    CURSOR=$MBEGIN-1
    zle -R
    zselect -t 50 -r 0 < /dev/tty
    CURSOR=$oldCURSOR
    region_highlight=($old_region_highlight)
  fi
}
zle -N showmatch
bindkey ']' showmatch
bindkey ')' showmatch
bindkey '}' showmatch

zle-line-pre-redraw() {
  emulate -L zsh
  set -o rematchpcre
  region_highlight=(${region_highlight:#* $SHOWMATCH_GR})
  local a=$BUFFER[CURSOR+1] b c MBEGIN o=0
  local -A pair=(
    ']' '['
    ')' '('
    '}' '{'
  )
  [[ -n "$RBUFFER" ]] &&
  case $a in
    ([[\({])
      b=${(k)pair[(re)$a]}
      [[ $RBUFFER =~ "^(\\$a(?:[^$b$a]++|(?1))*\\$b)" ]] && o=$CURSOR;;
    ([]\)}])
      b=$pair[$a]
      [[ $LBUFFER$a =~ "(\\$b(?:[^$a$b]++|(?1))*\\$a)\$" ]];;
    (*) return;;
  esac &&
    region_highlight+=("$((o+MBEGIN-1)) $((o+MEND)) $SHOWMATCH_GR")
}
zle -N zle-line-pre-redraw

# }}}

# {{{ custom hooks

cfg_custom_hook() {
    # Initialize homeshick
    source $HOME/.homesick/repos/homeshick/homeshick.sh

    # Import other bashrc config
    # this must be last to override everything else
    source $HOME/bin/import.sh

    add-zsh-hook precmd my_precmd_fn
    # add-zsh-hook preexec my_preexec_fn
    # add-zsh-hook chpwd my_chpwd_fn
}

# }}}

# zplug_setup
zinit_setup

}

# }}}

post_init

if [[ -n "${PROFILE_ZSH:-}" ]]; then
    zprof
fi
