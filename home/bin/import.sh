#!/bin/sh
IMPORTS=$HOME/imports

export DEBUG=

log() {
    if [[ ${DEBUG} == 1 ]]; then
        echo "[$(date +%H:%m:%S.%3N)] [*] $1"
    fi
}

run_import() {
    log "Starting imports..."

    if [ -d ${IMPORTS} ]; then
        for entry in "${IMPORTS}"/*.sh
        do
            source "${entry}"
            log "Imported: ${entry}"
        done
    fi

    log "Done with imports."
}

if [[ -z "${SKIP_IMPORT+x}" ]]; then
    run_import
fi
